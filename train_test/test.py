import torch
from torch.utils.data import DataLoader

def test_model(model, test_dataset, batch_size):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=True, num_workers=2)
    
    model.eval()  # Set the model to evaluation mode

    correct_predictions = 0
    total_samples = 0

    with torch.no_grad():
        for inputs, labels in test_loader:
            inputs, labels = inputs.to(device), labels.to(device)

            outputs = model(inputs)
            _, predicted = torch.max(outputs, 1)

            correct_predictions += (predicted == labels).sum().item()
            total_samples += labels.size(0)

    test_accuracy = correct_predictions / total_samples * 100.0
    print(f"Test Accuracy: {test_accuracy:.2f}%")

