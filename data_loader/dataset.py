import os
from torch.utils.data import Dataset
from torchvision import transforms
from torchvision.datasets import ImageFolder

class CustomDataset(Dataset):
    def __init__(self, root):
        self.transform = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
        ])
        self.dataset = ImageFolder(root=root, transform=self.transform)

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, index):
        return self.dataset[index]

