import torch
import torch.nn as nn

import os
from shutil import copyfile

import torch.nn.init as init
import random
import numpy as np


def create_train_test_sets(tx):
    # Set paths for train and test sets based on the translation value
    train_set_directory = f"/home/amine/equivariant-cnns/train_set_{tx}"
    test_set_directory = f"/home/amine/equivariant-cnns/test_set_{tx}"

    # Check if train and test directories already exist
    if os.path.exists(train_set_directory) and os.path.exists(test_set_directory):
        print("Train and test directories already exist.")
        return train_set_directory, test_set_directory

    # Create train and test directories
    os.makedirs(train_set_directory, exist_ok=True)
    os.makedirs(test_set_directory, exist_ok=True)

    categories = [17, 18, 19]
    for category_id in categories:
        category_directory = os.path.join(f"/home/amine/equivariant-cnns/translated_image_{tx}/", f"category_{category_id}")

        # Create folders for each category in training and testing sets
        train_category_directory = os.path.join(train_set_directory, f"category_{category_id}")
        test_category_directory = os.path.join(test_set_directory, f"category_{category_id}")

        os.makedirs(train_category_directory, exist_ok=True)
        os.makedirs(test_category_directory, exist_ok=True)

        images = os.listdir(category_directory)
        random.shuffle(images)

        split_ratio = 0.8
        split_index = int(len(images) * split_ratio)

        # Copy images to the train set
        for img in images[:split_index]:
            src_path = os.path.join(category_directory, img)
            dst_path = os.path.join(train_category_directory, img)
            copyfile(src_path, dst_path)

        # Copy images to the test set
        for img in images[split_index:]:
            src_path = os.path.join(category_directory, img)
            dst_path = os.path.join(test_category_directory, img)
            copyfile(src_path, dst_path)

    return train_set_directory, test_set_directory

def initialize_weights(model, seed=42):
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    np.random.seed(seed)
    random.seed(seed)

    # Initialize the model's weights
    for m in model.modules():
        if isinstance(m, (nn.Conv2d, nn.Linear)):
            init.xavier_uniform_(m.weight, gain=init.calculate_gain('relu'))
            if m.bias is not None:
                init.constant_(m.bias, 0)

from torch.utils.data import Dataset
from torchvision import transforms
from torchvision.datasets import ImageFolder

class CustomDataset(Dataset):
    def __init__(self, root):
        self.transform = transforms.Compose([
            # transforms.RandomRotation(degrees=15),
            # transforms.RandomAffine(translate=(0.1, 0.1), degrees=0,),    
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
        ])
        self.dataset = ImageFolder(root=root, transform=self.transform)

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, index):
        return self.dataset[index]
    
class CustomDatasetTest(Dataset):
    def __init__(self, root):
        self.transform = transforms.Compose([ 
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
        ])
        self.dataset = ImageFolder(root=root, transform=self.transform)

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, index):
        return self.dataset[index]
