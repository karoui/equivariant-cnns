import math
import torch
import torch.nn as nn
import numpy as np

class PositionalEncodingLayer(nn.Module):
    def __init__(self, num_channels):
        """
        Initialize the PositionalEncodingLayer module.
        
        :param channels: The number of channels in the input tensor (e.g., 3 for RGB images).
        """
        super(PositionalEncodingLayer, self).__init__()
        self.org_channels = num_channels
        num_channels = int(np.ceil(num_channels / 4) * 2)
        self.num_channels = num_channels
        inv_freq = 1.0 / (10000 ** (torch.arange(0, num_channels, 2).float() / num_channels))
        self.register_buffer("inv_freq", inv_freq)
        self.register_buffer("cached_penc", None, persistent=False)

    def forward(self, tensor):
        """
        Apply positional encoding to a 4D tensor (batch_size, x, y, channels).
        
        :param tensor: Input tensor of size (batch_size, x, y, channels).
        :return: Positional encoding matrix of size (batch_size, x, y, channels).
        """
        if len(tensor.shape) != 4:
            raise RuntimeError("The input tensor must be 4D (batch_size, x, y, channels)!")

        # if self.cached_penc is not None and self.cached_penc.shape == tensor.shape:
        #     return self.cached_penc

        batch_size, x, y, orig_ch = tensor.shape
        pos_x = torch.arange(x, device=tensor.device, dtype=self.inv_freq.dtype)
        pos_y = torch.arange(y, device=tensor.device, dtype=self.inv_freq.dtype)
        sin_inp_x = torch.einsum("i,j->ij", pos_x, self.inv_freq)
        sin_inp_y = torch.einsum("i,j->ij", pos_y, self.inv_freq)
        emb_x = self.get_emb(sin_inp_x).unsqueeze(1)
        emb_y = self.get_emb(sin_inp_y)
        emb = torch.zeros((x, y, self.num_channels * 2), device=tensor.device, dtype=tensor.dtype)
        emb[:, :, :self.num_channels] = emb_x
        emb[:, :, self.num_channels:2 * self.num_channels] = emb_y

        self.cached_penc = emb[None, :, :, :orig_ch].repeat(batch_size, 1, 1, 1)
        return tensor + self.cached_penc

    def get_emb(self, sin_inp):
        """
        Gets a base embedding for one dimension with sin and cos intertwined
        """
        emb = torch.stack((sin_inp.sin(), sin_inp.cos()), dim=-1)
        return torch.flatten(emb, -2, -1)
