import torch
import torch.nn as nn

class FourierEncodingLayer(nn.Module):
    def __init__(self):
        super(FourierEncodingLayer, self).__init__()

    def forward(self, x):
        fft_result = torch.fft.fftn(x, dim=(-2, -1))  # Apply FFT on spatial dimensions
        
        real_part = torch.real(fft_result)        
        return real_part