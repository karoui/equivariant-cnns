import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim.lr_scheduler import ReduceLROnPlateau


class BasicBlock(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1):
        super(BasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(out_channels)

    def forward(self, x):

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        out = self.relu(out)

        return out


class ResNetEncoder(nn.Module):
    def __init__(self, block, desired_layers, num_classes, use_global_pooling=True, learning_rate=0.001, epochs=45, patience_stop=4, weight_decay=1e-4, fourier_encoder_cls=None, laplacian_encoder_cls=None, positional_encoding_cls=None):
        super(ResNetEncoder, self).__init__()
        self.in_channels = 3
        self.block = block
        self.num_classes = num_classes
        self.use_global_pooling = use_global_pooling
        self.num_classes = num_classes

        self.learning_rate = learning_rate
        self.epochs = epochs
        self.patience_stop = patience_stop
        self.weight_decay = weight_decay

        self.fourier_encoder_cls = fourier_encoder_cls
        self.laplacian_encoder_cls = laplacian_encoder_cls
        self.positional_encoding_cls = positional_encoding_cls

        self.layers = nn.ModuleList()
        for i, layer_blocks in enumerate(desired_layers):
            stride = 2 if i > 0 else 1
            res_block = self.make_layer(self.block, 64 * (2 ** i), layer_blocks, stride)
            self.layers.append(res_block)

        self.use_global_pooling = self.use_global_pooling

        if self.use_global_pooling:
            self.global_pool = nn.AdaptiveAvgPool2d(1)

        final_out_channels = 64 * (2 ** (len(desired_layers) - 1))

        if laplacian_encoder_cls is not None:
            self.laplacian_encoder = self.laplacian_encoder_cls(out_channels=final_out_channels)

        if fourier_encoder_cls is not None:
            self.fourier_encoder = self.fourier_encoder_cls()

        if positional_encoding_cls is not None:
            self.positional_encoder = positional_encoding_cls(num_channels=final_out_channels)

        fc_input_size = 64 * (2 ** (len(desired_layers) - 1))
        
        if not self.use_global_pooling:
            fc_input_size *= 4*4

        self.fc = nn.Linear(fc_input_size, self.num_classes)


        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.Adam(self.parameters(), lr=self.learning_rate, weight_decay=self.weight_decay)
        self.scheduler = ReduceLROnPlateau(self.optimizer, mode='min', factor=0.1, patience=2, verbose=True)


    def make_layer(self, block, out_channels, blocks, stride):
        layer = []
        layer.append(block(self.in_channels, out_channels, stride))
        self.in_channels = out_channels
        for _ in range(1, blocks):
            layer.append(block(self.in_channels, out_channels, stride=1))
        return nn.Sequential(*layer)

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        if hasattr(self, 'fourier_encoder'):
            fourier_encoding = self.fourier_encoder(x)
            x = x + fourier_encoding

        if hasattr(self, 'laplacian_encoder'):
            x = x + self.laplacian_encoder(x)

        if hasattr(self, 'positional_encoder'):
            x = self.positional_encoder(x)

        if self.use_global_pooling:
            x = self.global_pool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x

    def fit(self, train_loader, val_loader):
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.to(device)
        self.criterion = nn.CrossEntropyLoss()

        best_val_loss = float('inf')
        best_val_accuracy = 0.0
        best_model_state = None
        patience_counter = 0

        training_results = {'train_accuracy': [], 'train_loss': [], 'val_accuracy': [], 'val_loss': []}

        for epoch in range(self.epochs):
            self.train()
            total_loss = 0
            correct_predictions = 0
            total_samples = 0

            for inputs, labels in train_loader:
                inputs, labels = inputs.to(device), labels.to(device)

                self.optimizer.zero_grad()
                outputs = self(inputs)
                loss = self.criterion(outputs, labels)

                # Applying weight decay
                for param in self.parameters():
                    loss += 1e-4 * torch.norm(param)

                loss.backward()

                # Gradient clipping to avoid exploding gradients
                nn.utils.clip_grad_norm_(self.parameters(), max_norm=1.0)

                self.optimizer.step()

                total_loss += loss.item()
                _, predicted = torch.max(outputs, 1)
                correct_predictions += (predicted == labels).sum().item()
                total_samples += labels.size(0)

            average_loss = total_loss / len(train_loader)
            accuracy = correct_predictions / total_samples * 100.0
            training_results['train_accuracy'].append(accuracy)
            training_results['train_loss'].append(average_loss)

            # Validation
            self.eval()
            val_loss = 0
            val_correct_predictions = 0
            val_total_samples = 0

            with torch.no_grad():
                for inputs, labels in val_loader:
                    inputs, labels = inputs.to(device), labels.to(device)
                    outputs = self(inputs)
                    val_loss += self.criterion(outputs, labels).item()
                    _, predicted = torch.max(outputs, 1)
                    val_correct_predictions += (predicted == labels).sum().item()
                    val_total_samples += labels.size(0)

            val_average_loss = val_loss / len(val_loader)
            val_accuracy = val_correct_predictions / val_total_samples * 100.0
            training_results['val_accuracy'].append(val_accuracy)
            training_results['val_loss'].append(val_average_loss)

            print(f"Epoch {epoch + 1}/{self.epochs}, Train Loss: {average_loss:.4f}, Train Accuracy: {accuracy:.2f}%, "
                f"Val Loss: {val_average_loss:.4f}, Val Accuracy: {val_accuracy:.2f}%")

            # Learning rate scheduling based on validation loss
            self.scheduler.step(val_average_loss)

            # Update best validation accuracy and model state
            if val_accuracy > best_val_accuracy:
                best_val_accuracy = val_accuracy
                best_model_state = self.state_dict()

            # Early stopping based on validation loss
            if val_average_loss < best_val_loss:
                best_val_loss = val_average_loss
                patience_counter = 0
            else:
                patience_counter += 1

            if patience_counter >= self.patience_stop:
                print(f"Early stopping at epoch {epoch + 1} as validation loss didn't improve for {self.patience_stop} epochs.")
                break

        # Restore the model state with the best validation accuracy
        if best_model_state is not None:
            self.load_state_dict(best_model_state)

        return training_results

    def predict(self, test_loader):
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.to(device)
        self.eval()
        
        predictions = []
        with torch.no_grad():
            for inputs in test_loader:
                inputs = inputs.to(device)
                outputs = self(inputs)
                _, predicted = torch.max(outputs, 1)
                predictions.extend(predicted.cpu().numpy())
        
        return predictions
