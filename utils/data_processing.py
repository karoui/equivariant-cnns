import cv2
from pycocotools.coco import COCO
import os
import numpy as np
import random
from shutil import copyfile

def create_translated_images(random_seed):
    # Set random seed for reproducibility
    random.seed(random_seed)
    np.random.seed(random_seed)

    # Path to your COCO annotations file and image directory
    annotations_path = '/home/amine/equivariant-cnns/coco_ann2017/annotations/instances_train2017.json'
    image_directory = '/home/amine/equivariant-cnns/content/train2017/'

    # Create COCO instance
    coco = COCO(annotations_path)

    # Output directory for translated images
    output_base_directory = '/home/amine/equivariant-cnns/translated_image_30/'

    # Translation parameters range
    tx_range = (-30, 30)
    ty_range = (-30, 30)

    # Create folders for each category
    categories = [17, 18, 19]
    for category_id in categories:
        category_directory = os.path.join(output_base_directory, f"category_{category_id}")
        os.makedirs(category_directory, exist_ok=True)

    # Get all image IDs
    image_ids = coco.getImgIds()

    # Iterate through each image
    for image_id in image_ids:
        # Load the image
        image_info = coco.loadImgs(image_id)[0]
        image_path = os.path.join(image_directory, image_info['file_name'])
        image = cv2.imread(image_path)

        # Load annotations for the selected image
        annotation_ids = coco.getAnnIds(imgIds=image_id)
        annotations = coco.loadAnns(annotation_ids)

        # Iterate through each annotation
        for annotation in annotations:
            category_id = coco.loadCats(annotation['category_id'])[0]['id']
            if category_id in categories:
                # Create a mask for the selected object
                mask = coco.annToMask(annotation)
                # Extract the object from the image using the mask
                object_image = cv2.bitwise_and(image, image, mask=mask)

                # Perform 5 random translations
                for i in range(5):
                    tx = random.uniform(*tx_range)
                    ty = random.uniform(*ty_range)

                    # Create a translation matrix
                    translation_matrix = np.float32([[1, 0, tx], [0, 1, ty]])

                    # Apply the translation to the mask
                    translated_mask = cv2.warpAffine(mask, translation_matrix, (mask.shape[1], mask.shape[0]))

                    # Apply the translation to the object image
                    translated_object_image = cv2.warpAffine(object_image, translation_matrix, (image.shape[1], image.shape[0]))

                    # Save the translated object image in the respective category folder
                    output_directory = os.path.join(output_base_directory, f"category_{category_id}")
                    output_path = os.path.join(output_directory, f"{image_id}_{i}.png")
                    cv2.imwrite(output_path, translated_object_image)

                    print(f"Translated object image saved in category {category_id}: {output_path}")


def create_translated_images_with_background(random_seed):
    # Set random seed for reproducibility
    random.seed(random_seed)
    np.random.seed(random_seed)

    # Path to your COCO annotations file and image directory
    annotations_path = '/home/amine/equivariant-cnns/coco_ann2017/annotations/instances_train2017.json'
    image_directory = '/home/amine/equivariant-cnns/content/train2017/'

    # Create COCO instance
    coco = COCO(annotations_path)

    # Output directory for translated images
    output_base_directory = '/home/amine/equivariant-cnns/translated_image_with_background_10/'

    # Translation parameters range
    tx_range = (-10, 10)
    ty_range = (-10, 10)

    # Create folders for each category
    categories = [17, 18, 19]
    for category_id in categories:
        category_directory = os.path.join(output_base_directory, f"category_{category_id}")
        os.makedirs(category_directory, exist_ok=True)

    # Get all image IDs
    image_ids = coco.getImgIds()

    # Iterate through each image
    for image_id in image_ids:
        # Load the image
        image_info = coco.loadImgs(image_id)[0]
        image_path = os.path.join(image_directory, image_info['file_name'])
        image = cv2.imread(image_path)

        # Load annotations for the selected image
        annotation_ids = coco.getAnnIds(imgIds=image_id)
        annotations = coco.loadAnns(annotation_ids)
        mask = np.zeros_like(image, dtype=bool)
        # Iterate through each annotation
        for annotation in annotations:
            category_id = coco.loadCats(annotation['category_id'])[0]['id']
            if category_id in categories:
                # Create a mask for the selected object and combine it with the existing mask
                annotation_mask = coco.annToMask(annotation).astype(bool)

                # Expand the mask to match the shape of the image
                mask = np.repeat(annotation_mask[:, :, np.newaxis], 3, axis=2)

                # Crop the object from the image using the mask
                object_image = image * mask

                # Generate background colors for the cropped object
                bg_color = generate_bg_colors(object_image)


                # Perform 5 random translations
                for i in range(5):
                    tx = int(random.uniform(*tx_range))
                    ty = int(random.uniform(*ty_range))

                    # Translate the mask and the object
                    translated_mask = np.roll(mask, (tx, ty), axis=(0, 1))
                    translated_object_image = np.roll(object_image, (tx, ty), axis=(0, 1))

                    # Create the final output image by combining the translated object and the background colors
                    output_image = translated_object_image + np.invert(translated_mask) * bg_color
                    output_directory = os.path.join(output_base_directory, f"category_{category_id}")
                    output_path = os.path.join(output_directory, f"{image_id}_{i}.png")
                    cv2.imwrite(output_path, output_image)

                    print(f"Translated object image saved in category {category_id}: {output_path}")


def generate_bg_colors(img, size=1, plot_channel_histograms=False, inverse_proba=True, proba_smoothing=1.1):
    bg_colors = []
    channels = ['red', 'green', 'blue']

    for channel in range(img.shape[-1]):
        probas, bins = np.histogram(img[:, :, channel], bins=32)
        bins = bins[:-1]
        probas = probas.astype(float) / np.size(img[:, :, channel])

        # Handle potential NaN values and division by zero
        inv_probas = np.ones_like(probas) if inverse_proba else probas
        nonzero_indices = probas != 0
        inv_probas[nonzero_indices] = 1 / probas[nonzero_indices] / np.sum(1 / probas[nonzero_indices])

        # Normalize probabilities to ensure they sum to 1
        inv_probas /= np.sum(inv_probas)

        #if plot_channel_histograms:
            #plt.bar(range(len(bins)), probas, alpha=.3, color=channels[channel])

        # choose ~ histogram
        color_val = np.random.choice(bins, size, p=inv_probas)

        # add some jitter so we get uniform sampling across the bins
        scale = np.max(img) / len(bins)
        color_val += np.random.uniform(0, scale, size)

        bg_colors.append(np.uint8(np.round(color_val)))

    if size == 1:
        return np.hstack(bg_colors)

    return np.transpose(bg_colors, (1, 2, 0))

def create_train_test_sets(tx):
    # Set paths for train and test sets based on the translation value
    train_set_directory = f"/home/amine/equivariant-cnns/train_set_{tx}"
    test_set_directory = f"/home/amine/equivariant-cnns/test_set_{tx}"

    # Check if train and test directories already exist
    if os.path.exists(train_set_directory) and os.path.exists(test_set_directory):
        print("Train and test directories already exist.")
        return train_set_directory, test_set_directory

    # Create train and test directories
    os.makedirs(train_set_directory, exist_ok=True)
    os.makedirs(test_set_directory, exist_ok=True)

    categories = [17, 18, 19]
    for category_id in categories:
        category_directory = os.path.join(f"/home/amine/equivariant-cnns/translated_image_{tx}/", f"category_{category_id}")

        # Create folders for each category in training and testing sets
        train_category_directory = os.path.join(train_set_directory, f"category_{category_id}")
        test_category_directory = os.path.join(test_set_directory, f"category_{category_id}")

        os.makedirs(train_category_directory, exist_ok=True)
        os.makedirs(test_category_directory, exist_ok=True)

        images = os.listdir(category_directory)
        random.shuffle(images)

        split_ratio = 0.8
        split_index = int(len(images) * split_ratio)

        # Copy images to the train set
        for img in images[:split_index]:
            src_path = os.path.join(category_directory, img)
            dst_path = os.path.join(train_category_directory, img)
            copyfile(src_path, dst_path)

        # Copy images to the test set
        for img in images[split_index:]:
            src_path = os.path.join(category_directory, img)
            dst_path = os.path.join(test_category_directory, img)
            copyfile(src_path, dst_path)

    return train_set_directory, test_set_directory