import os
import requests
from tqdm import tqdm
import zipfile

# Define URLs for the COCO dataset
coco_images_url = "http://images.cocodataset.org/zips/train2017.zip"
coco_annotations_url = "http://images.cocodataset.org/annotations/annotations_trainval2017.zip"

# Define the directory where you want to save the downloaded files
download_dir = "./home/amine/equivariant-cnns/content"

# Create the download directory if it doesn't exist
os.makedirs(download_dir, exist_ok=True)

# Download the COCO images zip file
coco_images_zip_path = os.path.join(download_dir, "coco_train2017.zip")
with requests.get(coco_images_url, stream=True) as response, open(coco_images_zip_path, "wb") as file:
    total_size = int(response.headers.get("content-length", 0))
    block_size = 1024
    for data in tqdm(response.iter_content(block_size), total=total_size // block_size, unit="KB", desc="Downloading COCO images"):
        file.write(data)

# Download the COCO annotations zip file
coco_annotations_zip_path = os.path.join(download_dir, "coco_annotations.zip")
with requests.get(coco_annotations_url, stream=True) as response, open(coco_annotations_zip_path, "wb") as file:
    total_size = int(response.headers.get("content-length", 0))
    block_size = 1024
    for data in tqdm(response.iter_content(block_size), total=total_size // block_size, unit="KB", desc="Downloading COCO annotations"):
        file.write(data)

# Extract the downloaded zip files
with zipfile.ZipFile(coco_images_zip_path, "r") as images_zip:
    images_zip.extractall(download_dir)

with zipfile.ZipFile(coco_annotations_zip_path, "r") as annotations_zip:
    annotations_zip.extractall(download_dir)

print("COCO dataset downloaded and extracted successfully.")

