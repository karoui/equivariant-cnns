from train_test.train import train_model
from train_test.test import test_model
from data_loader.dataset import CustomDataset
from models.resnet import ResNet, BasicBlock
from models.ResNetAblation import ResNetAblation, BasicBlock
from models.ResNetWithLaplacian import ResNetWithLaplacian
from models.ResNetWithFourier import ResNetWithFourier
from models.ResNetWithLaplacianAfterPooling import ResNetWithLaplacianAfterPooling
from models.ResNetWithFourierAfterPooling import ResNetWithFourierAfterPooling
from models.ResNetWithZernikeEncoding import ResNetWithZernikeEncoding
from utils.data_processing import create_translated_images, create_train_test_sets, create_translated_images_with_background
import torch

# Set random seed for reproducibility
random_seed = 42
create_translated_images(random_seed)

# Create train and test sets
train_set_directory, test_set_directory = create_train_test_sets(30)

# Hyperparameters
learning_rate = 0.001
batch_size = 32
epochs = 20

# Data preprocessing and loading
train_dataset = CustomDataset(root=train_set_directory)
test_dataset = CustomDataset(root=test_set_directory)

# Initialize the model, loss function, and optimizer with weight decay
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(torch.cuda.is_available())
print(torch.cuda.current_device())
print(torch.cuda.get_device_name(torch.cuda.current_device()))
#Pure pooling model
model = ResNet(BasicBlock, [2, 2, 2, 2, 2]).to(device)

#model = ResNetAblation(BasicBlock, [2, 2, 2, 2, 2]).to(device)
#spatial enncoding model
#model = ResNetWithZernikeEncoding(BasicBlock, [2, 2, 2], num_classes=4).to(device)
train_model(model, train_dataset, learning_rate, batch_size, epochs)

# Evaluate the trained model on the test set
test_model(model, test_dataset, batch_size)
